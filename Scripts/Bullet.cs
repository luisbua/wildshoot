﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public int NormalBarretlValue = 50;
    public int SpecialBarretlValue = 100;
    public AudioSource ImpactAudio;
    public AudioClip[] FXSouns;
   
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Diana")
        {
            ScoreScript.numDianas++;
            ScoreScript.bActivateFirstDiana = true;
        }

        if (collision.gameObject.tag == "NormalBarrel")
        {
            ScoreScript.ScoreBua += NormalBarretlValue;
        }
        else
        {
            if (collision.gameObject.tag == "SpecialBarrel")
            {
                ScoreScript.ScoreBua += SpecialBarretlValue;
            }
        }

        ImpactAudio.clip = FXSouns[Random.Range(0, FXSouns.Length - 1)];
        ImpactAudio.Play();
        
        Destroy(gameObject);
    }
}
