﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruction : MonoBehaviour
{

    public GameObject BreakVersion;
    public float ExplosionForce = 1f;
    protected Rigidbody rb;
    public float TimeToDestroy = 1f;
    

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //Destroy(gameObject, TimeToDestroy);
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    private void OnCollisionEnter(Collision collision)
    {
       

        Instantiate(BreakVersion, transform.position, transform.rotation);
        rb.AddExplosionForce(ExplosionForce, Vector3.zero, 0f);
        
        Destroy(gameObject);
    }

    
}
