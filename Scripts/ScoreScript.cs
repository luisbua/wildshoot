﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class ScoreScript : MonoBehaviour
{
    static public int ScoreBua = 0;
    static public int numDianas = 0;
    static public bool bActivateFirstDiana = false;
    static public bool GamePause = false;
    public float GameTime = 80f;
    public GameObject PausePanel;
    public GameObject ScorePanel;
    private bool bPause = false;
    private float GamePauseTime;
    public GameObject Diana;
    private bool bFirstDiana_ON = true;
    public Transform Diana1;
    public Transform Diana2;
    public float TimeDiana1 = 4f;
    private float StartTime = 0;
        
    // Start is called before the first frame update
    void Start()
    {
        PausePanel.SetActive(false);
        ScorePanel.SetActive(true);
        GamePauseTime = Time.time + GameTime;
        StartTime = Time.time;
        TimeDiana1 += StartTime;
        GamePause = false;
        ScoreBua = 0;
        numDianas = 0;
        bFirstDiana_ON = true;
        bActivateFirstDiana = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(Time.time > TimeDiana1 && bFirstDiana_ON)
        {
            bFirstDiana_ON = false;
            GameObject clone = (GameObject)Instantiate(Diana, Diana1.position, Diana1.rotation * Quaternion.Euler(0, 0, -90));
        }
        if(bActivateFirstDiana && numDianas < 2)
        {
            GameObject clone = (GameObject)Instantiate(Diana, Diana2.position, Diana2.rotation * Quaternion.Euler(0,0,-90));
            bActivateFirstDiana = false;
        }


        Debug.Log(Time.time);
        if(Time.time >= GamePauseTime && !bPause)
        {
            PauseGame();
            bPause = true;
            PausePanel.SetActive(true);
            //ScorePanel.SetActive(false);
            OVRScreenFade.instance.FadeOut();
            Invoke("ChangeLevel",3);
            //ChangeLevel();
           
        }

        if(Input.GetKey(KeyCode.P) || OVRInput.Get(OVRInput.Button.One, OVRInput.Controller.RTouch))
        {
            ResumeGame();
            PausePanel.SetActive(false);
            //ScorePanel.SetActive(true);
        }
    }

    void PauseGame()
    {
        GamePause = true;
        //Time.timeScale = 0f;
    }
    void ResumeGame()
    {
        GamePause = false;
        //Time.timeScale = 1f;
    }

    public void ChangeLevel()
    {
       SceneManager.LoadScene("EndGame", LoadSceneMode.Single);
    }
}
