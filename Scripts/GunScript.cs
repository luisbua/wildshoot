﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunScript : MonoBehaviour
{
    public GameObject projectile;
    public GameObject flare;
    public Transform FlarePosition;
    public Transform ProjectilePosition;
    public float SapwnRate = 0f;
    public float shootForce = 10f;
    public float RotationForce = 0f;
    public bool LeftHand = true;
    public AudioSource FireAuido;
    public AudioClip[] FXSounds; 
    private bool bTriggerPressed = false;
    private bool bTriggerReleased = true;
    private float Trigger = 0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        OVRInput.Update();

        if (LeftHand)
        {
            Trigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.LTouch);
        }
        else
        {
            Trigger = OVRInput.Get(OVRInput.Axis1D.PrimaryIndexTrigger, OVRInput.Controller.RTouch);
        }

        if (Trigger > 0.2f && bTriggerReleased)
        {
            bTriggerPressed = true;
            bTriggerReleased = false;
        }
        if (Trigger == 0f)
        {
            bTriggerReleased = true;
        }

        if (bTriggerPressed  && !ScoreScript.GamePause)
        {
            //Projectile
            GameObject go_projectile = (GameObject)Instantiate(projectile, ProjectilePosition.position, ProjectilePosition.rotation * Quaternion.Euler(0f, 90f, 0f));
            go_projectile.GetComponent<Rigidbody>().AddForce(ProjectilePosition.forward * shootForce);

            //Flare
            float rot = Random.Range(0f, 360f);
            GameObject go_flare = (GameObject)Instantiate(flare, FlarePosition.position, FlarePosition.rotation * Quaternion.Euler(rot, -90f, 0f));

            FireAuido.clip = FXSounds[Random.Range(0,FXSounds.Length - 1)];
            FireAuido.Play();

            bTriggerPressed = false;
        }

    }
}
