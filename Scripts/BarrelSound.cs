﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelSound : MonoBehaviour
{
    public AudioSource DestroytAudio;
    public AudioClip[] FXSouns;

    // Start is called before the first frame update
    void Start()
    {
        DestroytAudio.clip = FXSouns[Random.Range(0, FXSouns.Length - 1)];
        DestroytAudio.Play();
    }

    // Update is called once per frame
    void Update()
    {
    }
}
