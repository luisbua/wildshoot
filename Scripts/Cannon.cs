﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{
    public GameObject NormalBarrel;
    public GameObject SpecialBarrel;
    public Transform cannonTransform;
    private float TimeStamp = 0f;
    public float SpawnRate = 0f;
    private float RealSpawnRate = 0f;
    public float shootForce = 10f;
    public float RotationForce = 0f;
    public AudioSource FireAuido;
    public bool StartFire = false;
    private float RangeTime = 0f;

    // Start is called before the first frame update
    void Start()
    {
        RealSpawnRate = SpawnRate;
        RangeTime = (SpawnRate - 1.5f) / 5;
        StartFire = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(ScoreScript.numDianas >= 2)
        {
            StartFire = true;
        }

        if(Time.time < 10f)
        {
            RealSpawnRate = SpawnRate - RangeTime * 1;
        }
        else if (Time.time > 10f &&Time.time <= 40f)
        {
            RealSpawnRate = SpawnRate - RangeTime * 2;
        }
        else if (Time.time > 40f && Time.time <= 60f)
        {
            RealSpawnRate = SpawnRate - RangeTime * 3;
        }
        else if (Time.time > 60f && Time.time <= 80f)
        {
            RealSpawnRate = SpawnRate - RangeTime * 4;
        }
        else if(Time.time > 80f)
        {
            RealSpawnRate = SpawnRate - RangeTime * 5;
        }
       

        if (Time.time > TimeStamp && StartFire && !ScoreScript.GamePause)
        {
            GameObject clone;

            if (Random.Range(0f,100f) <= 10 )
            {
                clone = (GameObject)Instantiate(SpecialBarrel, cannonTransform.position, cannonTransform.rotation);
            }
            else
            {
                clone = (GameObject)Instantiate(NormalBarrel, cannonTransform.position, cannonTransform.rotation);
            }

            Vector3 var = new Vector3 (Random.Range(-100f,100f), Random.Range(-100f, 100f), Random.Range(-100f, 100f));

            clone.GetComponent<Rigidbody>().AddForce(cannonTransform.forward * (Random.Range(-1000f,1000f) + shootForce));
            clone.GetComponent<Rigidbody>().AddTorque(new Vector3(RotationForce + var.x, RotationForce + var.y, RotationForce + var.z),ForceMode.Impulse);

            FireAuido.Play();

            TimeStamp = Time.time + RealSpawnRate;
        }
    }
}
