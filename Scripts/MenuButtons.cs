﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuButtons : MonoBehaviour
{
    private GameObject Rectangle;
    private Material RecMat;
    private string LevelName;
    private ScreenFade sf;
    public void ChangeLevel(string mapName)
    {
        LevelName = mapName;
        OVRScreenFade.instance.FadeOut();

        Invoke("ChangingLevel", OVRScreenFade.instance.fadeTime);
        
       // SceneManager.LoadScene(mapName, LoadSceneMode.Single);

    }

    public void ActivateEmisiveMaterial()
    {
        //RecMat.EnableKeyword("_EMISSION");
    }

    public void DeactivateEmisiveMaterial()
    {
        //RecMat.DisableKeyword("_EMISSION");
    }

    // Start is called before the first frame update
    void Start()
    {
       RecMat = Rectangle.GetComponent<Renderer>().material;

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void ChangingLevel()
    {
        SceneManager.LoadScene(LevelName, LoadSceneMode.Single);
    }
}
